const { Schema, model } = require('mongoose');

const User = new Schema ({
    username: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    role: [{type: String, ref: 'Role'}],   
    created_date: {type: Date, default: Date.now()}   
});

module.exports = model('User', User);