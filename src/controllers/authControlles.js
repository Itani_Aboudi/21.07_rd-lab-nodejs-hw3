const User = require('../models/userModel');
const Role = require('../models/roleModel');
const bcrypt = require('bcrypt');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const {secret} = require('../../config');

const generateAccessToken = (id, roles) => {
    const payload = {
        id,
        roles
    }
    return jwt.sign(payload, secret, {expiresIn: '24h'})
}

class authController {
    async registration(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({message: 'Registration errors', errors});
            }
            const {username, password} = req.body;
            const candidate = await User.findOne({username});
            if (candidate) {
                return res.status(400).json({message: 'User already exists'});
            }
            const hashPassword = bcrypt.hashSync(password, 10);
            const userRole = await Role.findOne({value: 'USER'});
            const user = new User({username, password: hashPassword, role: [userRole.value]});
            await user.save();
            return res.status(200).json({message: "Profile created successfully"});

        } catch (err) {
            console.log(err);
            res.status(400).json({massage: 'Registration error'});
        }
    }

    async login(req, res) {
        try {
            const {username, password} = req.body;
            const user = await User.findOne({username});
            if (!user) {
                return res.status(400).json({message: `User with ${username} does not exist`});
            }
            const validPassword = bcrypt.compareSync(password, user.password);
            if (!validPassword) {
                return res.status(400).json({message: `Invalid password`});
            }
            const token = generateAccessToken(user._id, user.roles);
            return res.status(200).json({"jwt_token": token});
        } catch (err) {
            console.log(err);
            res.status(400).json({massage: 'Login error'});
        }
    }

    // async getUsers(req, res) {
    //     try {
    //         const users = await User.find();
    //         res.json(users);
    //     } catch (err) {
    //         console.log(err);
    //     }
    // }

    async getProfileInfo(req, res) {
        const {username, password} = req.body;
        console.log(username)
        // let user = req.user;
        // res.render('profile', { title: 'profile', user: user });
        // try {
        //     const username = req.body;
        //     console.log(username);
        //     const getUser = await User.findOne({username});
        //     if (!getUser) {
        //         return res.status(400).json({message: `User with ${username} does not exist`});
        //     }
        //     return res.status(200).json('hi');
        // } catch (err) {
        //     console.log(err);
        // }
    }
}

module.exports = new authController();