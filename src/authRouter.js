const Router = require('express');
const router = new Router();
const controller = require('./controllers/authControlles');
const {check} = require('express-validator');
const authMiddleware = require('./middlewares/authMiddlewear');
const roleMiddleware = require('./middlewares/roleMiddlewear');

router.post('/register', [
    check('username', 'User name is required').notEmpty(),
    check('password', 'Password must be longer then 4 and under 10').isLength({min: 4, max: 10})
], controller.registration);
router.post('/login', controller.login);
// router.get('/api/users/me', controller.getProfileInfo);
// router.get('/api/users/me', roleMiddleware(['USER']), controller.getProfileInfo);

module.exports = router;