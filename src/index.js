const express = require('express');
const morgan = require ('morgan');
const mongoose = require ('mongoose');

const authRouter = require('./authRouter');

const PORT = process.env.PORT || 8080;

const app = express();

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/auth', authRouter);


const start =  async () => {
    try {

       await mongoose.connect('mongodb+srv://itani:uber-fe-lab@cluster0.qkvqw.mongodb.net/ubertrucks?retryWrites=true&w=majority');
        app.listen(PORT, () => console.log(`Server listening at http://localhost:${PORT}/`));
    } catch (err) {
        console.error(err);
    }
}

start();